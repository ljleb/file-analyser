CREATE DATABASE Files;
USE Files;

CREATE TABLE File (
    id Int
        NOT null,

    magicNumber BigInt
        NOT null,

    magicType VarChar(7)
        NOT null,

    PRIMARY KEY (id)
);

CREATE TABLE Elf (
    fileId Int
        NOT null,

    class TinyInt
        NOT null
        CHECK (class in (32, 64, 0)),

    data Char(2)
        NOT null
        CHECK (LOWER(data) in ('le', 'be', 'nd')),

    type VarChar(4)
        NOT null
        CHECK (LOWER(type) in ('rel', 'exec', 'dyn', 'nd')),

    machine VarChar(5)
        NOT null
        CHECK (LOWER(machine) in ('x86', 'arm', 'amd64', 'nd')),

    phnum SmallInt
        NOT null,

    PRIMARY KEY (fileId),

    FOREIGN KEY (fileId)
        REFERENCES File (id)
);
