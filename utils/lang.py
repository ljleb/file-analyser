#!/usr/bin/env python3

class Withable:
    def __init__(self, this, exit_this):
        self.__this = this
        self.__exit_this = exit_this

    def __enter__(self):
        return self.__this

    def __exit__(self, type, value, traceback):
        self.__exit_this(self.__this)
        return None
