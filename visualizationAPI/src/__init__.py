#!/usr/bin/env python3

from flask import Flask, request, jsonify
from utils import config
from utils.sql import FilesDB

flask_app = Flask(__name__)

@flask_app.route('/list', methods=['GET'])
def index_list():
    with FilesDB() as database:
        file_ids = database.find_all_file_ids()

    return jsonify(file_ids)

@flask_app.route('/getinfo')
def index_get_info():
    file_id = request.args.get('fileid')
    if not file_id:
        return ('You must provide a value to the "fileid" argument', 404)

    response = {
        'fileId': file_id
    }

    with FilesDB() as database:
        update_type_info(file_id, response, database)
        update_elf_header(file_id, response, database)

    return jsonify(response)

def update_type_info(file_id, response, database):
    file_type = database.file_type_of(file_id)
    if file_type:
        response['fileType'] = file_type

def update_elf_header(file_id, response, database):
    elf_header = database.elf_header_of(file_id)
    if elf_header:
        response['elfHeader'] = elf_header

config.run_flask(flask_app, __name__)
