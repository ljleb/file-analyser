#!/usr/bin/env python3

import mysql.connector
from utils.lang import Withable
from utils import config

class FilesDB:
    def __init__(self):
        self.__connection = None

    def __enter__(self):
        crendentials = config.database_crendentials()
        self.__connection = mysql.connector.connect(**crendentials)
        return self

    def __exit__(self, type, value, traceback):
        self.__connection.close()
        self.__connection = None

    def find_next_file_id(self):
        with self.__withable_cursor() as cursor:
            cursor.execute(FilesDB.__find_next_file_id_query)
            new_file_id = next(id for (id, *_) in cursor.fetchall())
            return new_file_id + 1 if new_file_id else 1

    __find_next_file_id_query = ';'.join ([
        'SELECT MAX(id) FROM File'
    ])

    def store_file(self, file_id, file_first_bytes, file_type):
        with self.__withable_cursor() as cursor:
            magic_number = int.from_bytes(file_first_bytes, 'big', signed=False)
            query_arguments = (file_id, magic_number, file_type)
            cursor.execute(FilesDB.__store_file_query, query_arguments)
            self.__connection.commit()

    __store_file_query = ';'.join ([
        'INSERT INTO File (id, magicNumber, magicType) VALUES (%s, %s, %s)'
    ])

    def store_elf_contents(self, elf_info):
        query_arguments = {
            'fileId': elf_info.file_id,
            'class': elf_info.elf_class,
            'data': elf_info.elf_data,
            'type': elf_info.elf_type,
            'machine': elf_info.elf_machine,
            'phnum': elf_info.elf_phnum
        }

        with self.__withable_cursor() as cursor:
            cursor.execute(FilesDB.__store_elf_contents_query, query_arguments)
            self.__connection.commit()

    __store_elf_contents_query = ';'.join ([
        'INSERT INTO Elf (fileId, class, data, type, machine, phnum) VALUES (%(fileId)s, %(class)s, %(data)s, %(type)s, %(machine)s, %(phnum)s)'
    ])

    def find_all_file_ids(self):
        with self.__withable_cursor() as cursor:
            cursor.execute(FilesDB.__find_all_file_ids_query)
            return [id for (id, *_) in cursor.fetchall()]

    __find_all_file_ids_query = ';'.join([
        'SELECT id FROM File'
    ])

    def file_type_of(self, file_id):
        with self.__withable_cursor() as cursor:
            query_arguments = (file_id,)
            cursor.execute(FilesDB.__file_type_query, query_arguments)
            file_rows = cursor.fetchall()
            if file_rows:
                file_row = file_rows[0]
                return {
                    'magicNumber': hex(file_row[0])[2:],
                    'magicType': file_row[1]
                }

            else:
                return {}

    __file_type_query = ';'.join([
        'SELECT magicNumber, magicType FROM File WHERE id = %s'
    ])

    def elf_header_of(self, file_id):
        with self.__withable_cursor() as cursor:
            query_arguments = (file_id,)
            cursor.execute(FilesDB.__elf_header_query, query_arguments)
            elf_rows = cursor.fetchall()
            if elf_rows:
                elf_row = elf_rows[0]
                return {
                    'class': elf_row[0],
                    'data': elf_row[1],
                    'type': elf_row[2],
                    'machine': elf_row[3],
                    'phnum': elf_row[4]
                }

            else:
                return {}

    __elf_header_query = ';'.join([
        'SELECT class, data, type, machine, phnum FROM Elf WHERE fileId = %s'
    ])

    def __withable_cursor(self):
        return Withable(self.__connection.cursor(), lambda a: a.close())
