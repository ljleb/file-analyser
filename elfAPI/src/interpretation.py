#!/usr/bin/env python3

from utils import config
from utils.sql import FilesDB
from io import SEEK_CUR
import struct

def interpret_and_save_elf(file_id):
    file_path = config.analysis_path_of(file_id)
    with open(file_path, 'rb') as file:
        elf_contents = __interpret_elf_contents(file_id, file)

    with FilesDB() as database:
        database.store_elf_contents(elf_contents)

def __interpret_elf_contents(file_id, file):
    raw_contents = {}
    for raw_name, offset, length in __elf_known_offsets:
        raw_contents[raw_name] = __seek_and_read(file, offset, length)

    raw_contents['raw_phnum'] = {
        b'\x01': lambda: __seek_and_read(file, 0x2c - __elf_offsets_sum, 2),
        b'\x02': lambda: __seek_and_read(file, 0x38 - __elf_offsets_sum, 2)
    }.get(raw_contents['raw_class'], lambda: b'\x00\x00')()

    return interpret_raw_contents(file_id, **raw_contents)

def __seek_and_read(file, offset, size):
    if offset != 0:
        file.seek(offset, SEEK_CUR)
    return file.read(size)

__elf_known_offsets = [
    ('raw_class', 4, 1),
    ('raw_data', 0, 1),
    ('raw_type', 10, 2),
    ('raw_machine', 0, 2)
]

__elf_offsets_sum = sum({l + r for _, l, r in __elf_known_offsets})

def interpret_raw_contents(file_id, raw_class, raw_data, raw_type, raw_machine, raw_phnum):
    elf_class = __interpret_class(raw_class)
    elf_data = __interpret_data(raw_data)
    elf_type = __interpret_type(raw_type, elf_data)
    elf_machine = __interpret_machine(raw_machine, elf_data)
    elf_phnum = __interpret_phnum(raw_phnum, elf_data)

    return ElfInfo(file_id, elf_class, elf_data, elf_type, elf_machine, elf_phnum)

def __interpret_class(raw_class):
    parsed_class = struct.unpack('B', raw_class)[0]
    return {
        1: 32,
        2: 64,
    }.get(parsed_class, 0)

def __interpret_data(raw_data):
    parsed_data = struct.unpack('B', raw_data)[0]
    return {
        1: 'LE',
        2: 'BE'
    }.get(parsed_data, 'ND')

def __interpret_type(raw_type, elf_data):
    parsed_type = __parse_with_endianness(raw_type, elf_data)
    return {
        1: 'REL',
        2: 'EXEC',
        3: 'DYN'
    }.get(parsed_type, 'ND')

def __interpret_machine(raw_machine, elf_data):
    parsed_machine = __parse_with_endianness(raw_machine, elf_data)
    return {
        3: 'X86',
        40: 'ARM',
        62: 'AMD64'
    }.get(parsed_machine, 'ND')

def __interpret_phnum(raw_phnum, elf_data):
    return __parse_with_endianness(raw_phnum, elf_data)

def __parse_with_endianness(raw_info, elf_data):
    unpack_endianness = {
        'BE': '>',
        'LE': '<'
    }.get(elf_data, '')

    if unpack_endianness:
        unpack_format = unpack_endianness + 'H'
        return struct.unpack(unpack_format, raw_info)[0]

    else:
        return 0

class ElfInfo:
    def __init__(self, file_id, elf_class, elf_data, elf_type, elf_machine, elf_phnum):
        self.file_id = file_id
        self.elf_class = elf_class
        self.elf_data = elf_data
        self.elf_type = elf_type
        self.elf_machine = elf_machine
        self.elf_phnum = elf_phnum
