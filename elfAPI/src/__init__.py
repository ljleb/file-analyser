#!/usr/bin/env python3

from flask import Flask, request, jsonify
from utils import config
from interpretation import interpret_and_save_elf

flask_app = Flask(__name__)

@flask_app.route('/', methods=['GET'])
def index():
    file_id = request.args['file-id']
    interpret_and_save_elf(file_id)
    return jsonify({})

config.run_flask(flask_app, __name__)
