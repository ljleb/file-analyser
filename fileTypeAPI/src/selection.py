#!/usr/bin/env python3
from utils import config

def magic_bytes_of(file_id):
    file_path = config.analysis_path_of(file_id)
    with open(file_path, 'rb') as file_to_analyse:
        return file_to_analyse.read(8)

def find_file_type(file_magic_bytes):
    file_magic_number = file_magic_bytes.hex()
    for magic_number, file_type in __supported_magic_numbers.items():
        if file_magic_number.startswith(magic_number):
            return file_type

    return 'txt'

__supported_magic_numbers = {
    hex(k)[2:]: v for (k, v) in {
        0xa1b2c3d4:         'pcap',
        0xd4c3b2a1:         'pcap',
        0x0a0d0d0a:         'pcapng',
        0x474946383761:     'gif',
        0x474946383961:     'gif',
        0x6465780A30333500: 'dex',
        0x4d5a:             'exe',
        0x504b0304:         'zip',
        0x504b0506:         'zip',
        0x504b0708:         'zip',
        0x89504e470d0a1a0a: 'png',
        0xcafebabe:         'class',
        0x255044462d:       'pdf',
        0x424d:             'bmp',
        0xffd8ffd8:         'jpg',
        0xffd8ffee:         'jpg',
        0xffd8ffe0:         'jpg',
        0x4b444d:           'vmdk',
        0x7f454c46:         'elf',
        0xd0cf11e0a1b11ae1: 'doc',
        0x213c617263683e:   'deb',
        0x3c3f786d6c20:     'xml',
    }.items()
}